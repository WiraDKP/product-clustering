from pyclustering.cluster.kmedoids import kmedoids
from pyclustering.utils.metric import type_metric, distance_metric
from pyclustering.utils import read_sample;

from pyclustering.samples.definitions import FCPS_SAMPLES;
from pyclustering.cluster import cluster_visualizer;
from ComputeSimilarity import main
import json
import random

# with open('C:/Users/aprialdi.pratama/Downloads/sku_infoxx.json') as f:
#     sample = json.load(f)
# sample = read_sample(FCPS_SAMPLES.SAMPLE_LSUN)

#initial_medoids = [1, 2]
sample=[
    {"jumlah": [4, 3], "min_date": "2018-02-05 00:00:00+00:00", "max_date": "2018-03-14 00:00:00+00:00",
     "amount": 2583636.0, "cogs": 2559091.0, "category_name_level_1": "Aksesoris Gadget & Komputer",
     "category_name_level_2": "Storage Internal", "category_name_level_3": "Hard Disk Internal",
     "category_name_level_4": "Hdd Internal Sas 3.5 Inch", "brand": "HGST", "seri": "Deskstar NAS 6TB",
     "descriptions": "Deskstar NAS 6TB", "status": "Active & Publish", "sku": "0679297791"},
    {"jumlah": [2, 2, 1, 1], "min_date": "2018-01-15 00:00:00+00:00", "max_date": "2018-02-20 00:00:00+00:00",
     "amount": 4573382.0, "cogs": 4429272.75, "category_name_level_1": "Aksesoris Gadget & Komputer",
     "category_name_level_2": "Storage Internal", "category_name_level_3": "Hard Disk Internal",
     "category_name_level_4": "Hdd Internal Sas 3.5 Inch", "brand": "HGST", "seri": "Deskstar NAS 8TB",
     "descriptions": "Deskstar NAS 8TeraByte", "status": "Active & Publish", "sku": "0679297888"},
    {"jumlah": [1, 2, 2, 3], "min_date": "2018-01-15 00:00:00+00:00", "max_date": "2018-02-20 00:00:00+00:00",
     "amount": 4573082.0, "cogs": 4427572.75, "category_name_level_1": "Aksesoris Gadget & Komputer",
     "category_name_level_2": "Storage Internal", "category_name_level_3": "Hard Disk Internal",
     "category_name_level_4": "RAM Internal Sas 3.5 Inch", "brand": "HGST", "seri": "Deskstar NAS 8TB",
     "descriptions": "Deskstar NAS 8TB", "status": "Active & Publish", "sku": "0679297888"},
    {"jumlah": [3, 1, 1, 2], "min_date": "2018-01-15 00:00:00+00:00", "max_date": "2018-02-20 00:00:00+00:00",
     "amount": 2584636.0, "cogs": 2549091.0, "category_name_level_1": "Aksesoris Gadget & Komputer",
     "category_name_level_2": "Storage Internal", "category_name_level_3": "Hard Disk Internal",
     "category_name_level_4": "Hdd Internal Sas 3.5 Inch", "brand": "HGST", "seri": "Deskstar NAS 8TB",
     "descriptions": "RAM NAS 5TB", "status": "Active & Publish", "sku": "0679297888"}
]

def get_initial(n, max):
    result=[]
    for x in range(n):
        result.append(random.randint(1, max-1))
    return result

initial_medoids = get_initial(2, len(sample))
user_function = lambda point1, point2: main(point1,point2);
metric = distance_metric(type_metric.USER_DEFINED, func=user_function);
kmedoids_instance = kmedoids(sample, initial_medoids, metric=metric)
kmedoids_instance.process();
clusters = kmedoids_instance.get_clusters()
medoids = kmedoids_instance.get_medoids()

def get_top_n(n, clusters, medoids, dataset):
    results=[]
    for t in zip(clusters, medoids):
        members=t[0]
        medoid=t[1]
        dist=[]
        for m in members:
            distance=metric(dataset[m],dataset[medoid])
            dist.append((m, distance))
        dist.sort(key=lambda tup: tup[1])
        results.append(dist[:n])
    return results

result=get_top_n(1,clusters, medoids, sample)
print(result)

def membership(clusters, medoids, dataset):
    result=[]
    for t in zip(clusters, medoids):
        members=t[0]
        medoid=t[1]
        length=len(t[0])
        sum_distance=0
        for m in members:
            distance=metric(dataset[m],dataset[medoid])
            sum_distance+=distance
        average=sum_distance/float(length)
        result.append((members,average))
    return result

member=membership(clusters, medoids, sample)
print(member)

def visualize(clusters, initial, medoids):
    visualizer = cluster_visualizer(1);
    visualizer.append_clusters(clusters, sample, 0);
    visualizer.append_cluster([sample[index] for index in initial], marker='.', markersize=15);
    visualizer.append_cluster(medoids, data=sample, marker='*', markersize=15);
    visualizer.show()

#visualize(clusters,initial_medoids,medoids)
