from scipy.spatial.distance import euclidean
from fastdtw import fastdtw
import numpy as np
from pyclustering.utils.metric import type_metric, distance_metric;
from fuzzywuzzy import fuzz
import math
import spacy

#nlp=spacy.load('xx')
def normalized_timeseries(time):
    result=[]
    low=min(time)
    high=max(time)
    diff=float(high-low)
    for t in time:
        y=float(t-low)/diff
        result.append(y)
    return result

def compute_dtw_distance(timeseries1, timeseries2):
    #mean_data=(np.sum(timeseries1)+np.sum(timeseries2))/(len(timeseries1)+len(timeseries2))

    normalized1=normalized_timeseries(timeseries1)
    normalized2=normalized_timeseries(timeseries2)

    #norm_timeseries1=timeseries1/mean_data
    #norm_timeseries2=timeseries2/mean_data

    distance, path = fastdtw(normalized1, normalized2, dist=euclidean)
    return distance

def compute_price_distance(price1, price2):
    log1=math.log10(price1)
    log2=math.log10(price2)
    return math.fabs(log1-log2)

def compute_features_distances(features1, features2, distance_operators, weight):
    result={}
    for key, value1 in features1.items():
        value2=features2[key]
        op=distance_operators.get(key,None)
        if op:
            d=op(value1,value2)
            w=weight[key]
            result[key]=d*w

    return result

def compute_similarity(features_distances):
    result=0.0
    for key, value in features_distances.items():
        result=result+value

    return result

def compute_similarity_token_sort_ratio(txt1,txt2):
    return float(100-fuzz.token_sort_ratio(txt1, txt2))/100.0

#def compute_similarity_with_spacy(txt1, txt2):
	#return nlp(u"{}".format(txt1)).similarity(nlp(u"{}".format(txt2)))

def main(features1, features2):
    distance_operators = {'amount': compute_price_distance, 'cogs': compute_price_distance,
                          'jumlah': compute_dtw_distance, 'descriptions':compute_similarity_token_sort_ratio}
    weights = {'amount': 0.15, 'cogs': 0.15, 'jumlah': 0.5, 'descriptions':0.2}
    features_distance=compute_features_distances(features1, features2, distance_operators, weights)
    print('features distance', features_distance)
    similarity = compute_similarity(features_distance)
    return similarity

user_function = lambda point1, point2: main(point1,point2);
metric = distance_metric(type_metric.USER_DEFINED, func=user_function);
features1={"jumlah": [4, 3], "min_date": "2018-02-05 00:00:00+00:00", "max_date": "2018-03-14 00:00:00+00:00", "amount": 2583636.0, "cogs": 2559091.0, "category_name_level_1": "Aksesoris Gadget & Komputer", "category_name_level_2": "Storage Internal", "category_name_level_3": "Hard Disk Internal", "category_name_level_4": "Hdd Internal Sas 3.5 Inch", "brand": "HGST", "seri": "Deskstar NAS 6TB", "descriptions": "Deskstar NAS 6TB", "status": "Active & Publish", "sku": "0679297791"}
features2={"jumlah": [2, 2, 1, 1], "min_date": "2018-01-15 00:00:00+00:00", "max_date": "2018-02-20 00:00:00+00:00", "amount": 4573182.0, "cogs": 4427272.75, "category_name_level_1": "Aksesoris Gadget & Komputer", "category_name_level_2": "Storage Internal", "category_name_level_3": "Hard Disk Internal", "category_name_level_4": "Hdd Internal Sas 3.5 Inch", "brand": "HGST", "seri": "Deskstar NAS 8TB", "descriptions": "Deskstar NAS 8TB", "status": "Active & Publish", "sku": "0679297888"}
distance = metric(features1, features2)
print("Distance: ", distance)